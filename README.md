# About avc-commons-parent

This is the root project for Avantage Compris' open source Java projects.

Its parent project is [avc-base-parent](https://gitlab.com/avcompris/avc-base-parent/).

Projects that inherit from this one include:

  * [avc-commons-lang](https://gitlab.com/avcompris/avc-commons-lang/)
  * [avc-commons-testutil](https://gitlab.com/avcompris/avc-commons-testutil/)
  * [avc-binding-common](https://gitlab.com/avcompris/avc-binding-common/)
  * [avc-binding-dom](https://gitlab.com/avcompris/avc-binding-dom/)
  * [avc-binding-yaml](https://gitlab.com/avcompris/avc-binding-yaml/)

This is the project home page, hosted on GitLab.

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-commons-parent/)
