<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<!-- Copyright Avantage Compris SARL 2011-2015 © -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<artifactId>avc-commons-parent</artifactId>
	<version>0.6.3-SNAPSHOT</version>
	<packaging>pom</packaging>

	<parent>
		<groupId>net.avcompris.commons</groupId>
		<artifactId>avc-base-parent</artifactId>
		<version>0.6.2-SNAPSHOT</version>
	</parent>

	<dependencies>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<configuration>
					<encoding>UTF-8</encoding>
					<source>8</source>
					<detectJavaApiLink>false</detectJavaApiLink>
					<links>
						<link>https://docs.oracle.com/javase/8/docs/api/</link>
					</links>
				</configuration>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-report-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jxr-plugin</artifactId>
				<configuration>
					<inputEncoding>UTF-8</inputEncoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<configuration>
					<encoding>UTF-8</encoding>
					<source>8</source>
					<detectJavaApiLink>false</detectJavaApiLink>
					<links>
						<link>https://docs.oracle.com/javase/8/docs/api/</link>
					</links>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-pmd-plugin</artifactId>
				<configuration>
					<targetJdk>1.8</targetJdk>
					<sourceEncoding>UTF-8</sourceEncoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<configuration>
					<encoding>UTF-8</encoding>
					<!--
					<configLocation>https://maven.avcompris.com/checkstyle.xml</configLocation>
					-->
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>taglist-maven-plugin</artifactId>
				<configuration>
					<encoding>UTF-8</encoding>
					<sourceFileLocale>en</sourceFileLocale>
					<tags>
						<tag>TODO</tag>
						<tag>@todo</tag>
						<tag>FIXME</tag>
						<tag>XXX</tag>
						<tag>@deprecated</tag>
					</tags>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<configuration>
					<threshold>Ignore</threshold>
					<effort>Max</effort>
					<fork>true</fork>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>jdepend-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</reporting>

	<name>avc-commons-parent</name>
	<description>
		Root for all Avantage Compris' open source  Java projects.
	</description>
	<url>https://maven.avcompris.com/avc-commons-parent/</url>

	<licenses>
		<license>
			<name>Apache License Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo, manual</distribution>
			<comments>A business-friendly OSS license</comments>
		</license>
	</licenses>

	<scm>
		<connection>scm:git:https://gitlab.com/avcompris/avc-commons-parent.git</connection>
		<developerConnection>scm:git:git@gitlab.com:avcompris/avc-commons-parent.git</developerConnection>
		<url>https://gitlab.com/avcompris/avc-commons-parent</url>
		<tag>HEAD</tag>
	</scm>
	<ciManagement>
		<system>gitlab</system>
		<url>https://gitlab.com/avcompris/avc-commons-parent/-/pipelines</url>
	</ciManagement>

	<repositories>
		<repository>
			<id>OSSRH</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
	</repositories>

	<distributionManagement>
		<repository>
			<id>OSSRH</id>
			<name>Sonatype's Repository for releases</name>
			<url>
				https://oss.sonatype.org/service/local/staging/deploy/maven2
			</url>
			<uniqueVersion>false</uniqueVersion>
		</repository>
		<snapshotRepository>
			<id>OSSRH</id>
			<name>Sonatype's Repository for SNAPSHOTs</name>
			<url>
				https://oss.sonatype.org/content/repositories/snapshots
			</url>
			<uniqueVersion>false</uniqueVersion>
		</snapshotRepository>
		<site>
			<id>avcompris-sites</id>
			<url>scp://maven.avcompris.com/avc-commons-parent/</url>
		</site>
	</distributionManagement>
	
	<profiles>
		<profile>
			<id>mbpro</id>
			<properties>
				<WORKSPACE>/Users/dandriana/Documents/workspace</WORKSPACE>
			</properties>
		</profile>
	</profiles>

</project>
